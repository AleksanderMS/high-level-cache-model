import cache_functional_model
import pathlib
from lxml import etree

###### USER DEFINITIONS ######
trace_files = [
    'test_trace.txt'
]
memory_hierarchy_configs = [
    'example_cache_config.ini'
]
config_dir = '.'
trace_dir = './traces'
result_dir = '.'
##############################

#SETUP
memory_hierarchies = []
for config in memory_hierarchy_configs:
    memory_hierarchies.append(cache_functional_model.MemoryHierarchyModel(config_file="{}/{}".format(config_dir, config)))
xml_roots = []
for memory_hierarchy in memory_hierarchies:
    xml_roots.append(etree.Element(memory_hierarchy.model_name))

# RUN SIMULATION
for trace_file in trace_files:
    with open("./{}/{}".format(trace_dir, trace_file), "r") as fp:
        instruction = fp.readline()
        while instruction:
            address, write = instruction.strip().split(',')
            for memory_hierarchy in memory_hierarchies:
                memory_hierarchy.memory_access(address=int(address), write=int(write))
            instruction = fp.readline()
    # Gather results in respective xml containers
    for i, memory_hierarchy in enumerate(memory_hierarchies):
        trace = etree.SubElement(xml_roots[i], trace_file)
        for memory_level in memory_hierarchy.top_level_memories:
            level = etree.SubElement(trace, memory_level.model_name)
            etree.SubElement(level, "write_hits").text = "{}".format(memory_level.write_hits)
            etree.SubElement(level, "write_misses").text = "{}".format(memory_level.write_misses)
            etree.SubElement(level, "read_hits").text = "{}".format(memory_level.read_hits)
            etree.SubElement(level, "read_misses").text = "{}".format(memory_level.read_misses)
        for memory_level in memory_hierarchy.cache_memories:
            level = etree.SubElement(trace, memory_level.model_name)
            etree.SubElement(level, "write_hits").text = "{}".format(memory_level.write_hits)
            etree.SubElement(level, "write_misses").text = "{}".format(memory_level.write_misses)
            etree.SubElement(level, "read_hits").text = "{}".format(memory_level.read_hits)
            etree.SubElement(level, "read_misses").text = "{}".format(memory_level.read_misses)
        for memory_level in memory_hierarchy.main_memories:
            level = etree.SubElement(trace, memory_level.model_name)
            etree.SubElement(level, "write_hits").text = "{}".format(memory_level.write_hits)
            etree.SubElement(level, "write_misses").text = "{}".format(memory_level.write_misses)
            etree.SubElement(level, "read_hits").text = "{}".format(memory_level.read_hits)
            etree.SubElement(level, "read_misses").text = "{}".format(memory_level.read_misses)

# OUTPUT RESULTS TO FILES
for i, memory_hierarchy in enumerate(memory_hierarchies):
    path_output = pathlib.Path('./{}/{}.xml'.format(result_dir, memory_hierarchy.model_name))
    with path_output.open('wb+') as fp:
        fp.write(etree.tostring(xml_roots[i], pretty_print=True))
