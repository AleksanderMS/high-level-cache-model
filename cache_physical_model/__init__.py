import configparser
import pathlib


class CachePhysicalModel:
    """
    - Physical cache model based on data sheet values of energy access cost of sram memory blocks
    - TODO Add both active and idle dynamic energy usage
    """


    def __init__(self, model_name, result_dir):
        # Model Propertiese
        self.model_name = model_name
        self.result_dir = result_dir
        # Results
        self.read_hit_energy = 0
        self.read_miss_energy = 0
        self.write_hit_energy = 0
        self.write_miss_energy = 0
        self.leakage_power = 0
        # Working variables
        self.cache_name = ''
        self.associativity = 0
        self.access_mode = ''
        self.write_hit_policy = ''
        self.write_miss_policy = ''
        self.tag_ram_blocks = 0
        self.tag_dynamic_read_nj = 0
        self.tag_dynamic_write_nj = 0
        self.tag_leakage_power_uw = 0
        self.data_ram_blocks = 0
        self.data_banks = 0
        self.data_dynamic_read_nj = 0
        self.data_dynamic_write_nj = 0
        self.data_leakage_power_uw = 0
    

    def read_cache_properties(self, cache_name, config_file_path):
        config_parser = configparser.ConfigParser() # Parser for cache_config.ini file
        config_parser.read(config_file_path)
        self.cache_name = cache_name
        self.associativity = int(config_parser[self.cache_name]['associativity'])
        self.access_mode = config_parser[self.cache_name]['access_mode'] # parallel, sequential
        self.write_hit_policy  = config_parser[self.cache_name]['write_hit_policy'] # write_back, write_through, (write_invalidate?)
        self.write_miss_policy = config_parser[self.cache_name]['write_miss_policy'] # write_allocate, write_around
        self.tag_ram_blocks = int(config_parser[self.cache_name]['tag_ram_blocks']) # Total amount of ram blocks to store cache tags
        self.tag_dynamic_read = float(config_parser[self.cache_name]['tag_dynamic_read_nj'])
        self.tag_dynamic_write = float(config_parser[self.cache_name]['tag_dynamic_write_nj'])
        self.tag_leakage_power = float(config_parser[self.cache_name]['tag_leakage_power_uw'])
        self.data_ram_blocks = int(config_parser[self.cache_name]['data_ram_blocks']) # Total amount of ram blocks to store cache data
        self.data_banks = int(config_parser[self.cache_name]['data_banks']) # Amount of banks one data block (cache line) is divided into
        self.data_dynamic_read = float(config_parser[self.cache_name]['data_dynamic_read_nj'])
        self.data_dynamic_write = float(config_parser[self.cache_name]['data_dynamic_write_nj'])
        self.data_leakage_power = float(config_parser[self.cache_name]['data_leakage_power_uw'])

    
    def calculate_energy_usage(self):
        self._calc_read_energy()
        self._calc_write_energy()
        self._calc_leakage_power()


    def output_results(self):
        path_output = pathlib.Path('{}/{}_results.csv'.format(self.result_dir, self.model_name))
        if (path_output.is_file()):
            with path_output.open('a') as fp:
                fp.write('{}, {}, {}, {}, {}, {}'.format(self.cache_name, self.read_hit_energy, self.read_miss_energy, self.write_hit_energy, self.write_miss_energy, self.leakage_power))
                fp.write('\n')
        else:
            with path_output.open('w+') as fp:
                fp.write('Cache Name, Read Hit Energy (nJ), Read Miss Energy (nJ), Write Hit Energy (nJ), Write Miss Energy (nJ), Leakage Power (uW)\n')
                fp.write('{}, {}, {}, {}, {}, {}'.format(self.cache_name, self.read_hit_energy, self.read_miss_energy, self.write_hit_energy, self.write_miss_energy, self.leakage_power))
                fp.write('\n')


    def calculate_energy_usage_and_output_results(self):
        self.calculate_energy_usage()
        self.output_results()


    def _calc_read_energy(self):
        if (self.access_mode == 'sequential'):
            self.read_hit_energy  = (self.tag_dynamic_read * self.tag_ram_blocks) + self.data_dynamic_read
            self.read_miss_energy = (self.tag_dynamic_read * self.tag_ram_blocks) + self.tag_dynamic_write + (self.data_dynamic_write * self.data_banks)
        elif(self.access_mode == 'parallel'):
            self.read_hit_energy  = (self.tag_dynamic_read * self.tag_ram_blocks) + (self.data_dynamic_read * (self.data_ram_blocks / self.data_banks))
            self.read_miss_energy = (self.tag_dynamic_read * self.tag_ram_blocks) + (self.data_dynamic_read * (self.data_ram_blocks / self.data_banks)) + self.tag_dynamic_write + (self.data_dynamic_write * self.data_banks)


    def _calc_write_energy(self):
        # Write Hit Calculation
        if (self.write_hit_policy == 'write_back'):
            self.write_hit_energy = (self.tag_dynamic_read * self.tag_ram_blocks) + self.data_dynamic_write
        elif (self.write_hit_policy == 'write_through'):
            self.write_hit_energy = (self.tag_dynamic_read * self.tag_ram_blocks) + self.data_dynamic_write
        # Write Miss Calculation
        if (self.write_miss_policy == 'write_allocate'):
            self.write_miss_energy = (self.tag_dynamic_read * self.tag_ram_blocks) + self.tag_dynamic_write + (self.data_dynamic_write * self.data_banks)
        elif (self.write_miss_policy == 'write_around'):
            self.write_miss_energy = self.tag_dynamic_read * self.tag_ram_blocks


    def _calc_leakage_power(self):
        self.leakage_power = (self.tag_leakage_power + self.data_leakage_power) * self.associativity

