import configparser
import math
from numpy import random


class CacheLine:
    """
    - Cache Line struct
    """

    def __init__(self, address, write_back):
        self.address = address
        self.dirty = False


class CacheFunctionalModel:
    """
    - Functional cache model
    """

    def __init__(self, model_name, config_object):
        # Model properties
        self.model_name = model_name
        # Results
        self.read_hits = 0
        self.read_misses = 0
        self.write_hits = 0
        self.write_misses = 0
        # Physical properties
        self.size_bytes         = int(config_object['size_bytes'])
        self.block_size_bytes   = int(config_object['block_size_bytes'])
        self.associativity      = int(config_object['associativity']) 
        self.address_bits       = int(config_object['address_bits'])
        self.address_space      = [int(x.strip()) for x in config_object['address_space'].split(',')]
        if (self.associativity != 0):
            self.num_sets = int(self.size_bytes / (self.block_size_bytes * self.associativity))
            self.num_occupied_ways = [0 for i in range(self.num_sets)] # Occupied WAYS (Only used for replacement in Set-Associative Caches)
        else:
            self.num_sets = int(self.size_bytes / self.block_size_bytes)
            self.num_occupied_sets = 0 # Occupied SETS (Only used for replacement in Fully-Associative Caches)
        # Address bits division
        self.offset_bits = int(math.log(self.block_size_bytes, 2))
        if (self.associativity != 0):
            self.index_bits = int(math.log(self.num_sets, 2))
        else:
            self.index_bits = 0
        self.tag_bits = self.address_bits - self.index_bits - self.offset_bits
        # Cache policies
        self.replacement_policy = config_object['replacement_policy']
        self.write_hit_policy   = config_object['write_hit_policy']
        self.write_miss_policy  = config_object['write_miss_policy']
        # Tag array model
        if (self.associativity > 1): # set associative cache
            self.tag_array = [[CacheLine(address="NONE", write_back=self.write_hit_policy) for i in range(self.associativity)] for i in range(self.num_sets)] # All memory blocks contained by the cache level
        else:                   # fully associative and direct mapped
            self.tag_array = [CacheLine(address="NONE", write_back=self.write_hit_policy) for i in range(self.num_sets)]
        # Next memory level
        self.next_memory_levels = []
        # Set appropriate search and placement methods
        if (self.associativity == 0):
            self._search_tag_array = self._fully_assoc_search
            self._place_cache_line = self._fully_assoc_placement
        elif (self.associativity == 1):
            self._search_tag_array = self._direct_mapped_search
            self._place_cache_line = self._direct_mapped_placement
        else:
            self._search_tag_array = self._set_assoc_search
            self._place_cache_line = self._set_assoc_placement


    def add_next_memory_level(self, next_memory_level):
        self.next_memory_levels.append(next_memory_level)
        return


    def memory_access(self, address, write):
        if ((address >= self.address_space[0]) and (address <= self.address_space[1])):
            # Valid address space
            address_bits = "{}".format("{0:b}".format(address).zfill(self.address_bits))
            hit = self._search_tag_array(address_bits, write)
            if (hit):
                if (write == 1):
                    self.write_hits += 1
                    if (self.write_hit_policy == 'write-through'):
                        for next_memory_level in self.next_memory_levels:
                            next_memory_level.memory_access(address=address, write=1)
                else:
                    self.read_hits += 1
                return
            else:
                if (write == 1):
                    self.write_misses += 1
                else:
                    self.read_misses += 1
                for next_memory_level in self.next_memory_levels:
                    next_memory_level.memory_access(address=address, write=write)
                if ((write == 0) or (self.write_miss_policy == 'write-allocate')):
                    removed_cache_line = self._place_cache_line(address_bits)
                    if (removed_cache_line.dirty):
                        for next_memory_level in self.next_memory_levels:
                            next_memory_level.memory_access(address=int(removed_cache_line.address, 2), write=1)
                return
        else:
            # Not valid address space
            return


    def _fully_assoc_search(self, address, write):
        for i, cache_line in enumerate(self.tag_array):
            if (cache_line.address[0:self.tag_bits] == address[0:self.tag_bits]):
                # HIT
                if ((write == 1) and (self.write_hit_policy == 'write-back')):
                    cache_line.dirty = True
                if (self.replacement_policy == "lru"):
                    self.tag_array.insert(0, self.tag_array.pop(i))
                return True
        # MISS
        return False


    def _direct_mapped_search(self, address, write):
        index = int(address[self.tag_bits:self.tag_bits+self.index_bits], 2)
        if (self.tag_array[index].address[0:self.tag_bits] == address[0:self.tag_bits]):
            if ((write == 1) and (self.write_hit_policy == 'write-back')):
                self.tag_array[index].dirty = True
            return True
        return False


    def _set_assoc_search(self, address, write):
        index = int(address[self.tag_bits:self.tag_bits+self.index_bits], 2)
        for i, cache_line in enumerate(self.tag_array[index]):
            if (cache_line.address[0:self.tag_bits] == address[0:self.tag_bits]):
                if ((write == 1) and (self.write_hit_policy == 'write-back')):
                        cache_line.dirty = True
                if (self.replacement_policy == "lru"):
                    self.tag_array[index].insert(0, self.tag_array[index].pop(i))
                return True
        return False


    def _fully_assoc_placement(self, address):
        removed_cache_line = None
        if (self.num_occupied_sets == self.num_sets):
            if (self.replacement_policy == "random"):
                i = random.randint(self.num_sets)
                removed_cache_line = self.tag_array.pop(i)
            else: # FIFO, LRU
                removed_cache_line = self.tag_array.pop()
        else:
            removed_cache_line = self.tag_array.pop()
            self.num_occupied_sets += 1
        self.tag_array.insert(0, CacheLine(address, (self.write_hit_policy == 'write-back')))
        return removed_cache_line


    def _direct_mapped_placement(self, address):
        index = int(address[self.tag_bits:self.tag_bits+self.index_bits], 2)
        removed_cache_line = self.tag_array[index]
        self.tag_array[index].dirty = False
        self.tag_array[index].address = address
        return removed_cache_line


    def _set_assoc_placement(self, address):
        removed_cache_line = None
        index = int(address[self.tag_bits:self.tag_bits+self.index_bits], 2)
        if (self.num_occupied_ways[index] == self.associativity):
            if (self.replacement_policy == "random"):
                i = random.randint(self.associativity)
                removed_cache_line = self.tag_array[index].pop(i)
            else: # FIFO, LRU
                removed_cache_line = self.tag_array[index].pop()
        else:
            removed_cache_line = self.tag_array[index].pop()
            self.num_occupied_ways[index] += 1
        self.tag_array[index].insert(0, CacheLine(address, (self.write_hit_policy == 'write-back')))
        return removed_cache_line


class MainMemoryModel:
    """
    - Access counter for ram and rom memory
    """

    def __init__(self, model_name, config_object):
        self.model_name = model_name
        self.address_space = [int(x.strip()) for x in config_object['address_space'].split(',')]
        self.read_hits = 0
        self.read_misses = 0
        self.write_hits = 0
        self.write_misses = 0


    def memory_access(self, address, write):
        if ((address >= self.address_space[0]) and (address <= self.address_space[1])):
            if (write == 1):
                self.write_hits += 1
            else:
                self.read_hits += 1
        else:
            return


class MemoryHierarchyModel:
    """
    - Memory hierachy model of caches, ram and flash
    """

    def __init__(self, config_file):
        config_parser = configparser.ConfigParser(inline_comment_prefixes=';') # Parser for cache_config.ini file
        config_parser.read(config_file)
        self.model_name = config_parser['general']['hierarchy_name']
        memory_level_keys = config_parser.sections()
        self.top_level_keys = []
        self.main_memory_keys = []
        self.cache_keys = []
        for key in memory_level_keys:
            if (key == 'general'):
                pass
            elif (config_parser[key].getboolean('top_level')):
                self.top_level_keys.append(key)
            elif (config_parser[key]['memory_type'] == 'main_memory'):
                self.main_memory_keys.append(key)
            elif (config_parser[key]['memory_type'] == 'cache'):
                self.cache_keys.append(key)
        self.top_level_memories = []
        self.main_memories = []
        self.cache_memories = []
        for key in self.top_level_keys:
            self.top_level_memories.append(CacheFunctionalModel(model_name=key, config_object=config_parser[key]))
        for key in self.main_memory_keys:
            self.main_memories.append(MainMemoryModel(model_name=key, config_object=config_parser[key]))
        for key in self.cache_keys:
            self.cache_memories.append(CacheFunctionalModel(model_name=key, config_object=config_parser[key]))
        # SET NEXT LEVEL
        for memory_level in (self.top_level_memories + self.cache_memories):
            next_memory_levels = [x.strip() for x in config_parser[memory_level.model_name]['next_memory_levels'].split(',')]
            for lower_level in (self.cache_memories):
                if (lower_level.model_name in next_memory_levels):
                    memory_level.add_next_memory_level(lower_level)
            for lower_level in (self.main_memories):
                if (lower_level.model_name in next_memory_levels):
                    memory_level.add_next_memory_level(lower_level)


    def memory_access(self, address, write):
        for memory_level in self.top_level_memories:
            memory_level.memory_access(address, write)
        return
